const wrapper = document.querySelector('.wrapper');
const content = document.querySelector('.slider-track');
const scrollbarW = document.querySelector('.rails');
const scrollbar  = document.querySelector('.rail-thumb');
         
ratio = ((content.scrollWidth - scrollbarW.offsetWidth) / (scrollbarW.offsetWidth - scrollbar.offsetWidth));

scrollbar.onmousedown = function(event) {
  event.preventDefault();

  let shiftX = event.clientX - scrollbar.getBoundingClientRect().left;

  document.addEventListener('mousemove', onMouseMove);
  document.addEventListener('mouseup', onMouseUp);

  function onMouseMove(event) {
    let newLeft = event.clientX - shiftX - scrollbarW.getBoundingClientRect().left;

    if (newLeft < 0) {
      newLeft = 0;
    }
    let rightEdge = scrollbarW.offsetWidth - scrollbar.offsetWidth;
    if (newLeft > rightEdge) {
      newLeft = rightEdge;
    }
      
    scrollbar.style.left = newLeft + "px";
    content.style.transform = "translateX(" + -(newLeft * ratio) + "px)";
  }

  function onMouseUp() {
    document.removeEventListener('mouseup', onMouseUp);
    document.removeEventListener('mousemove', onMouseMove);
  }

};

scrollbar.ondragstart = function() {
  return false;
};
